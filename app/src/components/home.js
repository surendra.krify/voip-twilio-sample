import React, { Component} from "react";
import ReactDom from "react-dom";
import axios from "axios";
import openSocket from 'socket.io-client';
import {Twilio} from 'twilio';
const socket = openSocket('http://localhost:8080');

class Home extends Component{
    constructor(){
        super();
        this.state = {
            calls:[]
        }
    }
    componentDidMount(){

        axios.get('http://localhost:8080/token').then((res)=>{
            // console.log(res);
            Twilio.Device.setup(res.data);
        })
        socket.on('callComing', (data)=>{
            console.log('call coming');
            var array = this.state.calls;
            array.push(data.data);
            this.setState({calls:array});
        });
        // twilio.Device.incoming(function(conn) {
        //     console.log('incoming');
        //     console.log(conn);
        //     conn.accept();
        // })
    }
    render(){
        return(
            <div>  
                <h1>Home</h1> 
                {
                    this.state.calls.map((a)=>{
                        return(
                            <div key={a.CallSid} style={{backgroundColor:'red', padding:10}}>
                                <h1>{a.CallSid}</h1>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}


export default Home;

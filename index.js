let express = require('express');
let app = new express();
let JsSIP = require('jssip');

app.get('/',()=>{
    console.log('Hello World');
    // var socket = new JsSIP.WebSocketInterface('wss://sip.myhost.com');
    // var configuration = {
    //   sockets  : [ socket ],
    //   uri      : 'sip:alice@example.com',
    //   password : 'superpassword'
    // };

    // var ua = new JsSIP.UA(configuration);

    // ua.start();

    // // Register callbacks to desired call events
    // var eventHandlers = {
    //   'progress': function(e) {
    //     console.log('call is in progress');
    //   },
    //   'failed': function(e) {
    //     console.log('call failed with cause: '+ e.data.cause);
    //   },
    //   'ended': function(e) {
    //     console.log('call ended with cause: '+ e.data.cause);
    //   },
    //   'confirmed': function(e) {
    //     console.log('call confirmed');
    //   }
    // };

    // var options = {
    //   'eventHandlers'    : eventHandlers,
    //   'mediaConstraints' : { 'audio': true, 'video': true }
    // };

    // var session = ua.call('sip:bob@example.com', options);

    var socket = new JsSIP.WebSocketInterface('wss://sip.myhost.com');
    var configuration = {
      sockets  : [ socket ],
      uri      : 'sip:alice@example.com',
      password : 'superpassword'
    };
    var coolPhone = new JsSIP.UA(configuration);
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
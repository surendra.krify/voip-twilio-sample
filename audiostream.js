const WebSocket = require('ws');    
const express = require('express');
const { Start, Say } = require('twilio/lib/twiml/VoiceResponse');
const app = express();
const server = require('http').Server(app);
const wss = new WebSocket.Server({server:server});

const speech = require('@google-cloud/speech');
const client = new speech.SpeechClient();

const request = {
    config: {
        encoding: "MULAW",
        sampleRateHertz: 8000,
        languageCode: "en-US"
    },
    interimResults: true // If you want interim results, set this to true
}

wss.on('connection', (ws) => {
    console.log('Client connected');

    let recognizeStream = null;
    
    ws.on('message', (message) => {
        const msg = JSON.parse(message);
        switch(msg.event) {
            case "connected":
                console.log('connected');
                recognizeStream = client.streamingRecognize(request)
                .on('error', console.error)
                .on('data', data => {
                    console.log(data.results[0].alternatives[0].transcript);
                });
                break;
            case "start":
                console.log('started');
                break;
            case "media":
                console.log('media');
                recognizeStream.write(msg.media.payload);
                break;
            case "stop":
                console.log('stop');
                recognizeStream.destroy();
                break;
        }
    });
    ws.send('something');
})

app.post('/', (req, res) => {
    res.set('Content-Type', 'text/xml');
    res.send(
        `<Response>
            <Start>
                <Stream url="wss://${req.headers.host}" />
            </Start>
            <Say>I will stream in next 60 seconds</Say>
            <Pause length="60" />
        </Response>`
    )
})

console.log('Listening port at 8080');
server.listen(8080);